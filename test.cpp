#include "pch.h"
#include <deque>
#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>
#include "gtest/gtest.h"
#include <list>
#include <iterator>
#include <forward_list>
#include "test.h"

using namespace std;

bool operator<(const Data& lhs, const Data& rhs) {
    return lhs.y < rhs.y;
}

void callback(Data& d) {
    std::cout << "x = " << static_cast<int>(d.x) << ", y = " << d.y << endl;
}

bool compartor(const Data& d1, const Data& d2) 
{ 
    return d1.y < d2.y; 
}

template <typename Container>
void reorder_container(Container container, uint8_t x_threshold, size_t n, function<void(Data&)> callback) {

    for_each(container.begin(), container.end(), [](const Data& d) {
        cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
        });
    // Separate container into subsets A and B
    auto it = partition(container.begin(), container.end(), [x_threshold](const Data& d) { return d.x > x_threshold; });
    const auto subset_a_end = it;
    const auto subset_b_begin = it;

    if (it != container.end())
    {
        cout << "After partition" << endl;

        // Move subset A before subset B

        for_each(container.begin(), container.end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        // Sort subset C (first n elements of subset B) by y in ascending order
        partial_sort(subset_b_begin, subset_b_begin + n, container.end(), [](const Data& d1, const Data& d2) { return d1.y < d2.y; });
        cout << "After partial sort " << endl;
        for_each(container.begin(), container.end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        cout << "Callback" << endl;
        // Apply callback to subset C
        auto subset_c_end = subset_b_begin + n;
        if (subset_b_begin != container.end() && subset_c_end != container.end())
            for_each(subset_b_begin, subset_c_end, callback);

    }
  
    
}

template <typename Container>
void reorder_container(Container * container, uint8_t x_threshold, size_t n, function<void(Data&)> callback) {

    for_each(container->begin(), container->end(), [](const Data& d) {
        cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
        });
    // Separate container into subsets A and B
    auto it = partition(container->begin(), container->end(), [x_threshold](const Data& d) { return d.x > x_threshold; });
    const auto subset_a_end = it;
    const auto subset_b_begin = it;

    if (it != container->end())
    {
        cout << "After partition" << endl;

        // Move subset A before subset B

        for_each(container->begin(), container->end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        // Sort subset C (first n elements of subset B) by y in ascending order
        partial_sort(subset_b_begin, subset_b_begin + n, container->end(), [](const Data& d1, const Data& d2) { return d1.y < d2.y; });
        cout << "After partial sort " << endl;
        for_each(container->begin(), container->end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        cout << "Callback" << endl;
        // Apply callback to subset C
        auto subset_c_end = subset_b_begin + n;
        if (subset_b_begin != container->end() && subset_c_end != container->end())
            for_each(subset_b_begin, subset_c_end, callback);

    }


}

template <>
void reorder_container <std::list<Data>>(std::list<Data> container, uint8_t x_threshold, size_t n, function<void(Data&)> callback) {

    std::for_each(container.begin(), container.end(), [](const Data& d) {
        std::cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
        });
    // Separate container into subsets A and B
    auto it = partition(container.begin(), container.end(), [x_threshold](const Data& d) { return d.x > x_threshold; });
    auto subset_a_end = it;
    auto subset_b_begin = it;

    if (it != container.end())
    {
        auto subset_c_end = next(subset_b_begin, n);
        auto count = distance(container.begin(), it);
        std::cout << "After partition" << endl;

        // Move subset A before subset B
       // std::rotate(subset_a_end, subset_b_begin, container.end());
        std::for_each(container.begin(), container.end(), [](const Data& d) {
            std::cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        // Sort subset C (first n elements of subset B) by y in ascending order
        std::list<Data> temp;
        temp.splice(temp.end(), container, subset_b_begin, container.end());
        temp.sort(compartor);

        container.insert(container.end(), temp.begin(), temp.end());

        std::cout << "After partial sort " << endl;
        std::for_each(container.begin(), container.end(), [](const Data& d) {
            std::cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        std::cout << "Callback" << endl;
        // Apply callback to subset C
        if (count < container.size())
        {
            auto subset_c_begin = next(container.begin(), count);
            subset_c_end = next(subset_c_begin, n);
            for_each(subset_c_begin, subset_c_end, callback);
        }

    }
    
}

template <>
void reorder_container <std::list<Data>>(std::list<Data>* container, uint8_t x_threshold, size_t n, function<void(Data&)> callback) {

    std::for_each(container->begin(), container->end(), [](const Data& d) {
        std::cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
        });
    // Separate container into subsets A and B
    auto it = partition(container->begin(), container->end(), [x_threshold](const Data& d) { return d.x > x_threshold; });
    auto subset_a_end = it;
    auto subset_b_begin = it;

    if (it != container->end())
    {
        auto subset_c_end = next(subset_b_begin, n);
        auto count = distance(container->begin(), it);
        std::cout << "After partition" << endl;

        // Move subset A before subset B
       // std::rotate(subset_a_end, subset_b_begin, container.end());
        std::for_each(container->begin(), container->end(), [](const Data& d) {
            std::cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        // Sort subset C (first n elements of subset B) by y in ascending order
        std::list<Data> temp;
        temp.splice(temp.end(), *container, subset_b_begin, container->end());
        temp.sort(compartor);

        container->insert(container->end(), temp.begin(), temp.end());

        std::cout << "After partial sort " << endl;
        std::for_each(container->begin(), container->end(), [](const Data& d) {
            std::cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        std::cout << "Callback" << endl;
        // Apply callback to subset C
        if (count < container->size())
        {
            auto subset_c_begin = next(container->begin(), count);
            subset_c_end = next(subset_c_begin, n);
            for_each(subset_c_begin, subset_c_end, callback);
        }

    }

}


template <>
void reorder_container < std::forward_list<Data>>(std::forward_list<Data> container, uint8_t x_threshold, size_t n, function<void(Data&)> callback) {

    for_each(container.begin(), container.end(), [](const Data& d) {
        cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
        });
    // Separate container into subsets A and B
    auto it = partition(container.begin(), container.end(), [x_threshold](const Data& d) { return d.x > x_threshold; });
    auto subset_a_end = it;
    auto subset_b_begin = it;
    if (it != container.end())
    {
        auto subset_c_end = next(subset_b_begin, n);
        auto count = distance(container.begin(), it);
        subset_a_end = next(container.begin(), count - 1);
        cout << "After partition" << endl;

        // Move subset A before subset B
       // std::rotate(subset_a_end, subset_b_begin, container.end());
        for_each(container.begin(), container.end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        // Sort subset C (first n elements of subset B) by y in ascending order
        std::forward_list<Data> temp(subset_b_begin, container.end());
        temp.sort(compartor);
        container.erase_after(subset_a_end, container.end());
        container.insert_after(subset_a_end, temp.begin(), temp.end());

        cout << "After partial sort " << endl;
        for_each(container.begin(), container.end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        cout << "Callback" << endl;
        // Apply callback to subset C
        auto subset_c_begin = next(container.begin(), count);
        subset_c_end = next(subset_c_begin, n);
        for_each(subset_c_begin, subset_c_end, callback);
    }
    
}

template <>
void reorder_container < std::forward_list<Data>>(std::forward_list<Data>* container, uint8_t x_threshold, size_t n, function<void(Data&)> callback) {

    for_each(container->begin(), container->end(), [](const Data& d) {
        cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
        });
    // Separate container into subsets A and B
    auto it = partition(container->begin(), container->end(), [x_threshold](const Data& d) { return d.x > x_threshold; });
    auto subset_a_end = it;
    auto subset_b_begin = it;
    if (it != container->end())
    {
        auto subset_c_end = next(subset_b_begin, n);
        auto count = distance(container->begin(), it);
        subset_a_end = next(container->begin(), count - 1);
        cout << "After partition" << endl;

        // Move subset A before subset B
       // std::rotate(subset_a_end, subset_b_begin, container.end());
        for_each(container->begin(), container->end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        // Sort subset C (first n elements of subset B) by y in ascending order
        std::forward_list<Data> temp(subset_b_begin, container->end());
        temp.sort(compartor);
        container->erase_after(subset_a_end, container->end());
        container->insert_after(subset_a_end, temp.begin(), temp.end());


        cout << "After partial sort " << endl;
        for_each(container->begin(), container->end(), [](const Data& d) {
            cout << "x = " << static_cast<int>(d.x) << " , y = " << d.y << endl;
            });

        cout << "Callback" << endl;
        // Apply callback to subset C
        auto subset_c_begin = next(container->begin(), count);
        subset_c_end = next(subset_c_begin, n);
        for_each(subset_c_begin, subset_c_end, callback);
    }

}

TEST(ReOrderContainerTes1, operatortest)
{
    Data d1;
    d1.x = 10;
    d1.y = 6.7;
    Data d2;
    d2.x = 20;
    d2.y = 5.8;

    EXPECT_TRUE(d2 < d1);
}

TEST(ReOrderContainerTes1, operatortest2)
{
    Data d1;
    d1.x = 10;
    d1.y = 6.7;
    Data d2;
    d2.x = 20;
    d2.y = 5.8;

    EXPECT_FALSE(d1 < d2);
}

TEST(ReOrderContainerTes1, Partition)
{
    std::list<Data> container = {
        { 5, 2.0 },
        { 1, 4.0 },
        { 3, 1.0 },
        { 8, 3.0 },
        { 2, 5.0 },
        { 7, 0.5 }
    };

    auto pos = std::partition(container.begin(), container.end(), [](const Data& d) { return d.x > 3; });


    std::vector<int> res{ 5, 7, 8, 3, 2, 1 };
    auto it2 = res.begin();

    for (auto it = container.begin(); it != container.end() && it2 != res.end(); it++, it2++)
    {
        int lhs = static_cast<int>((*it).x);
        EXPECT_EQ(lhs, *it2);
    }


}


TEST(ReOrderContainerTes1, PartialSort)
{
    std::deque<Data> container = {
        { 5, 2.0 },
        { 7, 0.5 },
        { 8, 3.0 },
        { 3, 1.0 },
        { 2, 5.0 },
        { 1, 4.0 }
    };

    auto it = container.begin() + 2;
    std::partial_sort(it, it + 2, container.end(), [](const Data& d1, const Data& d2) { return d1.y < d2.y; });


    std::deque<int> res{ 5, 7, 3, 8, 2, 1 };
    auto it2 = res.begin();

    for (auto it = container.begin(); it != container.end() && it2 != res.end(); it++, it2++)
    {
        int lhs = static_cast<int>((*it).x);
        EXPECT_EQ(lhs, *it2);
    }


}


TEST(ReOrderContainerTes1, ReOrderContainer)
{
    std::vector<Data> container = {
       { 5, 2.0 },
       { 1, 4.0 },
       { 3, 1.0 },
       { 8, 3.0 },
       { 2, 5.0 },
       { 7, 0.5 }
    };
    
    reorder_container(&container, 3, 2, callback);

    std::vector<int> res{ 5, 7, 8, 3, 1, 2 };
    auto it2 = res.begin();

    for (auto it = container.begin(); it != container.end() && it2 != res.end(); it++, it2++)
    {
        int lhs = static_cast<int>((*it).x);
        EXPECT_EQ(lhs, *it2);
    }

}


TEST(ReOrderContainerTes1, ReOrderContainerDeque)
{
    std::deque<Data> container = {
       { 5, 2.0 },
       { 1, 4.0 },
       { 3, 1.0 },
       { 8, 3.0 },
       { 2, 5.0 },
       { 7, 0.5 }
    };

    reorder_container(&container, 3, 2, callback);
    std::deque<int> res{ 5, 7, 8, 3, 1, 2 };
    auto it2 = res.begin();

    for (auto it = container.begin(); it != container.end() && it2 != res.end(); it++, it2++)
    {
        int lhs = static_cast<int>((*it).x);
        EXPECT_EQ(lhs, *it2);
    }

}


TEST(ReOrderContainerTes1, ReOrderContainerList)
{
    std::list<Data> container = {
       { 5, 2.0 },
       { 1, 4.0 },
       { 3, 1.0 },
       { 8, 3.0 },
       { 2, 5.0 },
       { 7, 0.5 }
    };

    reorder_container(&container, 3, 2, callback);

    std::list<int> res{ 5, 7, 8, 3, 1, 2 };
    auto it2 = res.begin();

    for (auto it = container.begin(); it != container.end() && it2 != res.end(); it++, it2++)
    {
        int lhs = static_cast<int>((*it).x);
        EXPECT_EQ(lhs, *it2);
    }
}


TEST(ReOrderContainerTes1, ReOrderContainerList1)
{

    reorder_container(std::list<Data>{ {5, 2.0}, { 4,1.0 }}, 1, 1, callback);

}

TEST(ReOrderContainerTes1, ReOrderContainerForwardList)
{
    std::forward_list<Data> container = {
       { 5, 2.0 },
       { 1, 4.0 },
       { 3, 1.0 },
       { 8, 3.0 },
       { 2, 5.0 },
       { 7, 0.5 }
    };

    reorder_container(container, 3, 2, callback);

}


TEST(ReOrderContainerTes1, ReOrderContainerForwardList1)
{
    std::forward_list<Data> container = {
       { 5, 2.0 },
       { 1, 4.0 },
       { 3, 1.0 },
       { 8, 3.0 },
       { 2, 5.0 },
       { 7, 0.5 }
    };

    reorder_container(&container, 3, 2, callback);

}