#pragma once
#include <iostream>
#include <functional>
#include <algorithm>

struct Data {
    uint8_t x;
    double y;
};


bool operator<(const Data& lhs, const Data& rhs);
void callback(Data& d);
template <typename Container>
void reorder_container(Container container, uint8_t x_threshold, size_t n, std::function<void(Data&)> callback);
template <typename Container>
void reorder_container(Container* container, uint8_t x_threshold, size_t n, std::function<void(Data&)> callback);
template <>
void reorder_container < std::list<Data>>(std::list<Data> container, uint8_t x_threshold, size_t n, std::function<void(Data&)> callback);
template <>
void reorder_container < std::list<Data>>(std::list<Data>* container, uint8_t x_threshold, size_t n, std::function<void(Data&)> callback);
template <>
void reorder_container < std::forward_list<Data>>(std::forward_list<Data> container, uint8_t x_threshold, size_t n, std::function<void(Data&)> callback);
template <>
void reorder_container < std::forward_list<Data>>(std::forward_list<Data>* container, uint8_t x_threshold, size_t n, std::function<void(Data&)> callback);